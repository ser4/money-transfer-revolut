package che.revolut.transfer;

import che.revolut.transfer.dto.TransactionUpdateDto;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("test")
public class HookTestController {
    private static final List<TransactionUpdateDto> DTOS = new ArrayList<>();

    @POST
    @Path("hook")
    @Consumes(MediaType.APPLICATION_JSON)
    public void listenForUpdates(TransactionUpdateDto dto) {
        DTOS.add(dto);
        System.out.println("Recieved webhook update: " + dto);
    }

    public static void clearDtos() {
        DTOS.clear();
    }

    public static List<TransactionUpdateDto> readRecievedUpdates() {
        return DTOS;
    }
}
