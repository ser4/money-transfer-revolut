package che.revolut.transfer.transaction;

import che.revolut.transfer.TestUtil;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import che.revolut.transfer.transaction.service.TransactionExecutor;
import che.revolut.transfer.transaction.service.TransactionService;
import che.revolut.transfer.transaction.service.TransactionValidator;
import che.revolut.transfer.transaction.service.lock.TransactionLockService;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.concurrent.Executors;

import static org.mockito.Mockito.*;

public class TransactionProcessorTest {

    private final TransactionService transactionService = Mockito.mock(TransactionService.class);
    private final TransactionLockService transactionLockService = Mockito.mock(TransactionLockService.class);
    private final TransactionValidator transactionValidator = Mockito.mock(TransactionValidator.class);
    private final TransactionExecutor transactionExecutor = Mockito.mock(TransactionExecutor.class);

    private final TransactionProcessor underTest = new TransactionProcessor(transactionService, transactionLockService,
            transactionValidator, transactionExecutor, Executors.newSingleThreadExecutor());

    private final ArgumentCaptor<Runnable> r = ArgumentCaptor.forClass(Runnable.class);

    @Test
    public void shouldExecuteTransaction() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);
        when(transactionValidator.validate(update)).thenReturn(true);

        // when
        underTest.process(update);

        // then
        verify(transactionLockService).executeWithLock(eq(update), r.capture());
        r.getValue().run();
        verify(transactionExecutor).execute(update);
    }

    @Test
    public void shouldNotValidateTransactionWithoutLock() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);

        // when
        underTest.process(update);

        // then
        verify(transactionLockService).executeWithLock(eq(update), r.capture());
        verifyZeroInteractions(transactionExecutor);
        verifyZeroInteractions(transactionValidator);
    }

    @Test
    public void shouldNotExecuteTransaction() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);
        when(transactionValidator.validate(update)).thenReturn(false);

        // when
        underTest.process(update);

        // then
        verify(transactionLockService).executeWithLock(eq(update), r.capture());
        r.getValue().run();
        verifyZeroInteractions(transactionExecutor);
    }
}