package che.revolut.transfer.transaction.service.lock;

import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import che.revolut.transfer.transaction.service.TransactionService;
import org.junit.Test;
import org.mockito.Mockito;

import static che.revolut.transfer.TestUtil.update;
import static org.mockito.Mockito.*;

public class TransactionLockServiceTest {
    private final AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
    private final TransactionService transactionService = Mockito.mock(TransactionService.class);
    private final LockAcquirer lockAcquirer = Mockito.mock(LockAcquirer.class);

    private final TransactionLockService underTest = new TransactionLockService(transactionService, lockAcquirer, accountRepository);

    @Test
    public void shouldLockAndReleaseIfAvailable() {
        // given
        final TransactionUpdate update = update(1, 2, 3, 4, Status.CREATED);
        when(lockAcquirer.acquire(update)).thenReturn(true);

        // when
        underTest.executeWithLock(update, () -> {
        });

        // then
        verify(transactionService).updateTransaction(update, Status.LOCKED);
        verify(transactionService).updateTransaction(update, Status.UNLOCKED);
        verify(accountRepository).releaseLock(1, 2);
    }

    @Test
    public void shouldTryLockIfNotAvailable() {
        // given
        final TransactionUpdate update = update(1, 2, 3, 4, Status.CREATED);
        when(lockAcquirer.acquire(update)).thenReturn(false);

        // when
        underTest.executeWithLock(update, () -> {
        });

        // then
        verify(transactionService).failTransaction(update, ProblemCode.NOT_LOCKED);
        verify(transactionService, never()).updateTransaction(update, Status.UNLOCKED);
        verify(transactionService, never()).updateTransaction(update, Status.UNLOCKED);
        verifyNoMoreInteractions(accountRepository);
    }
}