package che.revolut.transfer.transaction.service;


import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import org.junit.Test;
import org.mockito.Mockito;

import static che.revolut.transfer.TestUtil.update;
import static org.mockito.Mockito.verify;

public class TransactionExecutorTest {
    private final AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
    private final TransactionService transactionService = Mockito.mock(TransactionService.class);
    private final TransactionExecutor underTest = new TransactionExecutor(accountRepository, transactionService);

    @Test
    public void shouldExecuteTransaction() {
        // given
        final TransactionUpdate update = update(1, 2, 3, 4, Status.VALIDATED);

        // when
        underTest.execute(update);

        // then
        verify(accountRepository).transferMoney(1, 2, 3);
        verify(transactionService).updateTransaction(update, Status.COMPLETED);
    }
}