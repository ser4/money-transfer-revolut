package che.revolut.transfer.transaction.service;

import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static che.revolut.transfer.TestUtil.update;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TransactionValidatorTest {
    private final AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
    private final TransactionService transactionService = Mockito.mock(TransactionService.class);

    private final TransactionValidator underTest = new TransactionValidator(accountRepository, transactionService);

    @Test
    public void shouldValidateRequest() {
        // given
        final TransactionUpdate update = update(1, 2, 3, 4, Status.LOCKED);
        when(accountRepository.validateTransfer(1, 2, 3)).thenReturn(Optional.empty());

        // when
        final boolean result = underTest.validate(update);

        // then
        assertThat(result).isTrue();
        verify(transactionService).updateTransaction(update, Status.VALIDATED);
    }

    @Test
    public void shouldNotValidateRequest() {
        // given
        final TransactionUpdate update = update(1, 2, 3, 4, Status.LOCKED);
        when(accountRepository.validateTransfer(1, 2, 3)).thenReturn(Optional.of(ProblemCode.SAME_ACCOUNT));

        // when
        final boolean result = underTest.validate(update);

        // then
        assertThat(result).isFalse();
        verify(transactionService).failTransaction(update, ProblemCode.SAME_ACCOUNT);
    }
}