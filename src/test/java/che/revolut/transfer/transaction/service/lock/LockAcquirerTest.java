package che.revolut.transfer.transaction.service.lock;

import che.revolut.transfer.TestUtil;
import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class LockAcquirerTest {

    private final AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
    private final LockAcquirer underTest = new LockAcquirer(accountRepository, 2, 1);

    @Test
    public void shouldAcquireLock() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);
        when(accountRepository.tryLock(1, 2)).thenReturn(true);

        // when
        final boolean result = underTest.acquire(update);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldNotAcquireLock() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);
        when(accountRepository.tryLock(1, 2)).thenReturn(false);

        // when
        final boolean result = underTest.acquire(update);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldNotAcquireLockAfter3Tries() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);
        when(accountRepository.tryLock(1, 2)).thenReturn(false, false, true);

        // when
        final boolean result = underTest.acquire(update);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldAcquireLockAfter2Tries() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);
        when(accountRepository.tryLock(1, 2)).thenReturn(false, true);

        // when
        final boolean result = underTest.acquire(update);

        // then
        assertThat(result).isTrue();
    }
}