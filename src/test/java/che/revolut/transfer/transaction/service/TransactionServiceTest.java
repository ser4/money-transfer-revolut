package che.revolut.transfer.transaction.service;

import che.revolut.transfer.TestUtil;
import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import che.revolut.transfer.transaction.repo.TransactionRepository;
import org.junit.Test;
import org.mockito.Mockito;

public class TransactionServiceTest {

    private final TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
    private final WebHookSender webHookSender = Mockito.mock(WebHookSender.class);

    private final TransactionService underTest = new TransactionService(transactionRepository, webHookSender);

    @Test
    public void shouldCreateTransaction() {
        // given
        final TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setAccountFrom(1);
        transactionRequest.setAccountTo(2);

        // when
        underTest.createTransaction(transactionRequest);

        // then
        Mockito.verify(transactionRepository).createTransaction(transactionRequest);
    }

    @Test
    public void shouldFailTransaction() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);

        // when
        underTest.failTransaction(update, ProblemCode.NOT_LOCKED);

        // then
        final TransactionUpdate expected = TestUtil.update(1, 2, 3, 4, Status.PROBLEM, ProblemCode.NOT_LOCKED);
        Mockito.verify(webHookSender).send(expected);
        Mockito.verify(transactionRepository).updateTransaction(expected);
    }

    @Test
    public void shouldUpdateAndSendTransactionUpdate() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);

        // when
        underTest.updateTransaction(update, Status.VALIDATED);

        // then
        final TransactionUpdate expected = TestUtil.update(1, 2, 3, 4, Status.VALIDATED);
        Mockito.verify(webHookSender).send(expected);
        Mockito.verify(transactionRepository).updateTransaction(expected);
    }

    @Test
    public void shouldUpdateAndNotSendTransactionUpdate() {
        // given
        final TransactionUpdate update = TestUtil.update(1, 2, 3, 4, Status.CREATED);

        // when
        underTest.updateTransaction(update, Status.LOCKED);

        // then
        final TransactionUpdate expected = TestUtil.update(1, 2, 3, 4, Status.LOCKED);
        Mockito.verifyZeroInteractions(webHookSender);
        Mockito.verify(transactionRepository).updateTransaction(expected);
    }
}