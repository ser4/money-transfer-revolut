package che.revolut.transfer.transaction;

import che.revolut.transfer.TestUtil;
import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import che.revolut.transfer.transaction.repo.TransactionInMemoryRepository;
import org.junit.Test;

import java.util.List;

import static che.revolut.transfer.TestUtil.request;
import static che.revolut.transfer.TestUtil.update;
import static org.assertj.core.api.Assertions.assertThat;

public class TransactionInMemoryRepositoryTest {

    private TransactionInMemoryRepository underTest = new TransactionInMemoryRepository();

    @Test
    public void shouldReadAllIds() {
        // given
        final TransactionRequest request = TestUtil.request(1, 2, 3);
        underTest.createTransaction(request);
        underTest.createTransaction(request);

        // when
        final List<Integer> result = underTest.readAllIds();

        // then
        assertThat(result).containsExactlyInAnyOrder(1, 2);
    }

    @Test
    public void shouldReadById() {
        // given
        final TransactionRequest request = TestUtil.request(1, 2, 3);
        underTest.createTransaction(request);

        // when
        final List<TransactionUpdate> result = underTest.readById(1);

        // then
        assertThat(result).containsExactly(update(1, 2, 3, 1, Status.CREATED));
    }

    @Test
    public void shouldReadEmptyListForNonExistingTransaction() {
        // given

        // when
        final List<TransactionUpdate> result = underTest.readById(1);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    public void shouldCreateTransaction() {
        // given
        final TransactionRequest request = TestUtil.request(1, 2, 3);

        // when
        final TransactionUpdate result = underTest.createTransaction(request);

        // then
        assertThat(result).isEqualTo(update(1, 2, 3, 1, Status.CREATED));
    }


    @Test
    public void shouldUpdateTransaction() {
        // given
        final TransactionUpdate update = update(1, 2, 3, 1, Status.LOCKED);
        underTest.createTransaction(request(1, 2, 3));

        // when
        underTest.updateTransaction(update);

        // then
        final List<TransactionUpdate> result = underTest.readById(1);
        assertThat(result).containsExactly(update(1, 2, 3, 1, Status.CREATED),
                update(1, 2, 3, 1, Status.LOCKED));
    }

}