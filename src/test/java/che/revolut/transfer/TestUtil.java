package che.revolut.transfer;

import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;

public class TestUtil {

    public static TransactionRequest request(int from, int to, int amount) {
        return request(from, to, amount, null);
    }

    public static TransactionRequest request(int from, int to, int amount, String hook) {
        final TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setAccountFrom(from);
        transactionRequest.setAccountTo(to);
        transactionRequest.setAmountInCents(amount);
        transactionRequest.setWebHookUrl(hook);
        return transactionRequest;
    }

    public static TransactionUpdate update(int from, int to, int amount, int transactionId, Status status) {
        final TransactionRequest request = request(from, to, amount);
        return new TransactionUpdate(transactionId, status, null, request);
    }

    public static TransactionUpdate update(int from, int to, int amount, int transactionId, Status status, ProblemCode problemCode) {
        final TransactionRequest request = request(from, to, amount);
        return new TransactionUpdate(transactionId, status, problemCode, request);
    }
}
