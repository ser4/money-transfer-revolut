package che.revolut.transfer;

import che.revolut.transfer.dto.AccountDto;
import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.dto.TransactionUpdateDto;
import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;


public class MainTest {
    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() {
        // start the server
        server = Main.startServer(Main.BASE_URI);
        // create the client
        final Client c = ClientBuilder.newClient();

        target = c.target(Main.BASE_URI);
    }

    @After
    public void tearDown() {
        server.shutdown();
    }

    @Test
    public void testServerIsUp() {
        // given

        // when
        final String result = target.path("bank").request().get(String.class);
        assertThat(result).isEqualTo("Server is Up!");
    }

    @Test
    public void shouldGetAccount() {
        // given

        // when
        final AccountDto result = target.path("bank/account/1").request(MediaType.APPLICATION_JSON).get(AccountDto.class);

        //then
        assertThat(result.getId()).isEqualTo(1);
        assertThat(result.getBalanceInCents()).isEqualTo(1000);
    }

    @Test
    public void shouldTransferMoneyAndSendUpdatesToWebHook() {
        // given
        HookTestController.clearDtos();
        final TransactionRequest request = TestUtil.request(2, 3, 100, "http://localhost:8080/test/hook/");

        // when
        final TransactionUpdateDto result = target.path("bank/transfer").request(MediaType.APPLICATION_JSON)
                .post(Entity.json(request), TransactionUpdateDto.class);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final AccountDto account2 = target.path("bank/account/2").request(MediaType.APPLICATION_JSON).get(AccountDto.class);
        final AccountDto account3 = target.path("bank/account/3").request(MediaType.APPLICATION_JSON).get(AccountDto.class);

        //then
        assertThat(result).isNotNull();
        assertThat(account2.getBalanceInCents()).isEqualTo(1900);
        assertThat(account3.getBalanceInCents()).isEqualTo(3100);

        final int transactionId = result.getTransactionId();

        final GenericType<List<TransactionUpdateDto>> genericType = new GenericType<List<TransactionUpdateDto>>() {
        };
        final List<TransactionUpdateDto> dtos = target.path("bank/transaction/" + transactionId)
                .request(MediaType.APPLICATION_JSON).get(genericType);

        assertThat(dtos).containsExactlyInAnyOrder(dto(transactionId, Status.CREATED), dto(transactionId, Status.LOCKED),
                dto(transactionId, Status.VALIDATED), dto(transactionId, Status.COMPLETED), dto(transactionId, Status.UNLOCKED));

        final List<TransactionUpdateDto> hookDtos = HookTestController.readRecievedUpdates();
        assertThat(hookDtos).containsExactlyInAnyOrder(dto(transactionId, Status.VALIDATED), dto(transactionId, Status.COMPLETED));

    }

    @Test
    public void shouldNotValidateTransactionAndSendUpdatesToWebHook() {
        // given
        HookTestController.clearDtos();
        final TransactionRequest request = TestUtil.request(2, 999, 100, "http://localhost:8080/test/hook/");

        // when
        final TransactionUpdateDto result = target.path("bank/transfer").request(MediaType.APPLICATION_JSON)
                .post(Entity.json(request), TransactionUpdateDto.class);

        final AccountDto account2 = target.path("bank/account/2").request(MediaType.APPLICATION_JSON).get(AccountDto.class);

        //then
        assertThat(result).isNotNull();
        assertThat(account2.getBalanceInCents()).isEqualTo(2000);

        final int transactionId = result.getTransactionId();

        final GenericType<List<TransactionUpdateDto>> genericType = new GenericType<List<TransactionUpdateDto>>() {
        };
        final List<TransactionUpdateDto> dtos = target.path("bank/transaction/" + transactionId)
                .request(MediaType.APPLICATION_JSON).get(genericType);

        assertThat(dtos).containsExactlyInAnyOrder(dto(transactionId, Status.CREATED), dto(transactionId, Status.LOCKED),
                dto(transactionId, Status.PROBLEM, ProblemCode.NOT_EXISTING_TO_ACCOUNT), dto(transactionId, Status.UNLOCKED));

        final List<TransactionUpdateDto> hookDtos = HookTestController.readRecievedUpdates();
        assertThat(hookDtos).containsExactlyInAnyOrder(dto(transactionId, Status.PROBLEM, ProblemCode.NOT_EXISTING_TO_ACCOUNT));

    }

    private TransactionUpdateDto dto(int transactionId, Status status) {
        return new TransactionUpdateDto(TestUtil.update(1, 1, 1, transactionId, status));
    }

    private TransactionUpdateDto dto(int transactionId, Status status, ProblemCode problemCode) {
        return new TransactionUpdateDto(TestUtil.update(1, 1, 1, transactionId, status, problemCode));
    }

    @Test(timeout = 10000)
    public void shouldTransferMoneyConcurrently() throws InterruptedException {
        // given
        final TransactionRequest request = TestUtil.request(5, 4, 10, "http://localhost:8080/test/hook/");
        final ExecutorService executor = Executors.newCachedThreadPool();

        final List<Callable<TransactionUpdateDto>> callables = IntStream.range(0, 100)
                .mapToObj(i -> (Callable<TransactionUpdateDto>) () ->
                        target.path("bank/transfer").request(MediaType.APPLICATION_JSON)
                                .post(Entity.json(request), TransactionUpdateDto.class))
                .collect(Collectors.toList());

        // when
        executor.invokeAll(callables);

        // waiting all to finish
        while (HookTestController.readRecievedUpdates().size() != 200) {
            Thread.sleep(100);
        }

        // then
        final AccountDto account4 = target.path("bank/account/4").request(MediaType.APPLICATION_JSON).get(AccountDto.class);
        final AccountDto account5 = target.path("bank/account/5").request(MediaType.APPLICATION_JSON).get(AccountDto.class);

        assertThat(account4.getBalanceInCents()).isEqualTo(5000);
        assertThat(account5.getBalanceInCents()).isEqualTo(4000);
    }
}