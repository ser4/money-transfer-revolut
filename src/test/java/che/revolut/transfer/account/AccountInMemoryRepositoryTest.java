package che.revolut.transfer.account;

import che.revolut.transfer.transaction.model.ProblemCode;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.assertj.core.api.Assertions.assertThat;


public class AccountInMemoryRepositoryTest {

    private final AccountInMemoryRepository underTest = new AccountInMemoryRepository();
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Test
    public void shouldReadAllAccounts() {
        // given

        // when
        final List<Integer> result = underTest.readAllIds();

        // then
        assertThat(result).containsExactlyInAnyOrder(1, 2, 3, 4, 5);
    }

    @Test
    public void shouldReadById() {
        // given

        // when
        final Optional<Account> result = underTest.readById(1);

        // then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getId()).isEqualTo(1);
        assertThat(result.get().getBalanceInCents()).isEqualTo(1000);
    }

    @Test
    public void shouldReturnEmptyForNotExisting() {
        // given

        // when
        final Optional<Account> result = underTest.readById(999);

        // then
        assertThat(result.isPresent()).isFalse();
    }

    @Test
    public void shouldLockAccounts() {
        // given

        // when
        final boolean result = underTest.tryLock(1, 2);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldNotLockAccounts() throws ExecutionException, InterruptedException {
        // given


        // when
        final boolean lock = underTest.tryLock(1, 2);
        final boolean result = executorService.submit(() -> underTest.tryLock(2, 3)).get();


        // then
        assertThat(lock).isTrue();
        assertThat(result).isFalse();
    }

    @Test
    public void shouldLockReleasedAccounts() throws ExecutionException, InterruptedException {
        // given

        // when
        final boolean lock = underTest.tryLock(1, 2);
        underTest.releaseLock(1, 2);
        final boolean result = executorService.submit(() -> underTest.tryLock(2, 3)).get();

        // then
        assertThat(lock).isTrue();
        assertThat(result).isTrue();
    }

    @Test
    public void shouldTransferMoney() {
        // given

        // when
        underTest.transferMoney(1, 2, 100);
        final Account accountFrom = underTest.readById(1).get();
        final Account accountTo = underTest.readById(2).get();


        // then
        assertThat(accountFrom.getBalanceInCents()).isEqualTo(900);
        assertThat(accountTo.getBalanceInCents()).isEqualTo(2100);
    }

    @Test
    public void shouldValidateTransfer() {
        // given

        // when
        final Optional<ProblemCode> result = underTest.validateTransfer(1, 2, 100);

        // then
        assertThat(result.isPresent()).isFalse();
    }

    @Test
    public void shouldValidateTransferForExactAmount() {
        // given

        // when
        final Optional<ProblemCode> result = underTest.validateTransfer(1, 2, 1000);

        // then
        assertThat(result.isPresent()).isFalse();
    }

    @Test
    public void shouldNotValidateForNotExistingFrom() {
        // given

        // when
        final Optional<ProblemCode> result = underTest.validateTransfer(999, 2, 100);

        // then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(ProblemCode.NOT_EXISTING_FROM_ACCOUNT);
    }

    @Test
    public void shouldNotValidateForNotExistingTo() {
        // given

        // when
        final Optional<ProblemCode> result = underTest.validateTransfer(1, 999, 100);

        // then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(ProblemCode.NOT_EXISTING_TO_ACCOUNT);
    }

    @Test
    public void shouldNotValidateForSameAccount() {
        // given

        // when
        final Optional<ProblemCode> result = underTest.validateTransfer(1, 1, 100);

        // then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(ProblemCode.SAME_ACCOUNT);
    }

    @Test
    public void shouldNotValidateForNotEnoughMoney() {
        // given

        // when
        final Optional<ProblemCode> result = underTest.validateTransfer(1, 2, 2000);

        // then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(ProblemCode.NOT_ENOUGH_MONEY);
    }

    @Test
    public void shouldNotValidateForZero() {
        // given

        // when
        final Optional<ProblemCode> result = underTest.validateTransfer(1, 2, 0);

        // then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(ProblemCode.NEGATIVE_OR_ZERO_AMOUNT);
    }

    @Test
    public void shouldNotValidateForNegative() {
        // given

        // when
        final Optional<ProblemCode> result = underTest.validateTransfer(1, 2, -10);

        // then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(ProblemCode.NEGATIVE_OR_ZERO_AMOUNT);
    }
}