package che.revolut.transfer.account;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class AccountTest {

    @Test
    public void shouldAddAmountToBalance() {
        // given
        final Account account = new Account(1, 100);

        // when
        account.changeBalance(5);

        // then
        assertThat(account.getBalanceInCents()).isEqualTo(105);
    }

    @Test
    public void shouldSubtractAmountFromBalance() {
        // given
        final Account account = new Account(1, 100);

        // when
        account.changeBalance(-5);

        // then
        assertThat(account.getBalanceInCents()).isEqualTo(95);
    }
}