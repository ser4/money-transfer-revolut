package che.revolut.transfer.account;

import che.revolut.transfer.transaction.model.ProblemCode;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import static che.revolut.transfer.transaction.model.ProblemCode.*;
import static java.util.Optional.of;

public class AccountInMemoryRepository implements AccountRepository {
    private final Map<Integer, Account> accountMap = new ConcurrentHashMap<>();
    private final Map<Integer, ReentrantLock> locks = new HashMap<>();


    public AccountInMemoryRepository() {
        accountMap.put(1, new Account(1, 1000));
        accountMap.put(2, new Account(2, 2000));
        accountMap.put(3, new Account(3, 3000));
        accountMap.put(4, new Account(4, 4000));
        accountMap.put(5, new Account(5, 5000));
    }


    @Override
    public boolean tryLock(int idAccountFrom, int idAccountTo) {
        final ReentrantLock lockFrom = getOrCreateLock(idAccountFrom);
        if (lockFrom.tryLock()) {
            final ReentrantLock lockTo = getOrCreateLock(idAccountTo);
            if (lockTo.tryLock()) {
                return true;
            }
            lockFrom.unlock();
        }
        return false;
    }

    private ReentrantLock getOrCreateLock(int idAccount) {
        locks.putIfAbsent(idAccount, new ReentrantLock());
        return locks.get(idAccount);
    }

    @Override
    public void releaseLock(int idAccountFrom, int idAccountTo) {
        locks.get(idAccountTo).unlock();
        locks.get(idAccountFrom).unlock();
    }

    @Override
    public List<Integer> readAllIds() {
        return new ArrayList<>(accountMap.keySet());
    }

    @Override
    public Optional<Account> readById(int id) {
        return Optional.ofNullable(accountMap.get(id));
    }

    @Override
    public void transferMoney(int idAccountFrom, int idAccountTo, int amountInCents) {
        final Account accountFrom = accountMap.get(idAccountFrom);
        final Account accountTo = accountMap.get(idAccountTo);

        accountFrom.changeBalance(-amountInCents);
        accountTo.changeBalance(amountInCents);
    }

    @Override
    public Optional<ProblemCode> validateTransfer(int idAccountFrom, int idAccountTo, int amountInCents) {
        if (amountInCents <= 0) {
            return of(NEGATIVE_OR_ZERO_AMOUNT);
        }

        final Account accountFrom = accountMap.get(idAccountFrom);
        if (accountFrom == null) {
            return of(NOT_EXISTING_FROM_ACCOUNT);
        }

        final Account accountTo = accountMap.get(idAccountTo);
        if (accountTo == null) {
            return of(NOT_EXISTING_TO_ACCOUNT);
        }
        if (idAccountFrom == idAccountTo) {
            return of(SAME_ACCOUNT);
        }

        if (accountFrom.getBalanceInCents() < amountInCents) {
            return of(NOT_ENOUGH_MONEY);
        }

        return Optional.empty();
    }
}

