package che.revolut.transfer.account;

import java.util.Objects;

public class Account {

    private final int id;
    private int balanceInCents;

    Account(int id, int balanceInCents) {
        this.id = id;
        this.balanceInCents = balanceInCents;
    }

    public int getId() {
        return id;
    }

    public int getBalanceInCents() {
        return balanceInCents;
    }

    public void changeBalance(int amountInCents) {
        balanceInCents += amountInCents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
