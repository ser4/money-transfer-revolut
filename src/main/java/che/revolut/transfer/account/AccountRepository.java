package che.revolut.transfer.account;

import che.revolut.transfer.transaction.model.ProblemCode;

import java.util.List;
import java.util.Optional;

public interface AccountRepository {

    boolean tryLock(int idAccountFrom, int idAccountTo);

    void releaseLock(int idAccountFrom, int idAccountTo);

    List<Integer> readAllIds();

    Optional<Account> readById(int id);

    void transferMoney(int idAccountFrom, int idAccountTo, int amountInCents);

    Optional<ProblemCode> validateTransfer(int idAccountFrom, int idAccountTo, int amountInCents);
}
