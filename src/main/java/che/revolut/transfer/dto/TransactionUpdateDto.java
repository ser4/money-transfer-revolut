package che.revolut.transfer.dto;

import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.Objects;

public class TransactionUpdateDto {
    private int transactionId;
    private Status status;
    private ProblemCode problemCode;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Date date;

    public TransactionUpdateDto(TransactionUpdate transactionUpdate) {
        this.transactionId = transactionUpdate.getTransactionId();
        this.status = transactionUpdate.getStatus();
        this.problemCode = transactionUpdate.getProblemCode();
        this.date = transactionUpdate.getDate();
    }

    public TransactionUpdateDto() {
    }

    public int getTransactionId() {
        return transactionId;
    }

    public Status getStatus() {
        return status;
    }

    public ProblemCode getProblemCode() {
        return problemCode;
    }

    public Date getDate() {
        return date;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setProblemCode(ProblemCode problemCode) {
        this.problemCode = problemCode;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionUpdateDto that = (TransactionUpdateDto) o;
        return transactionId == that.transactionId &&
                status == that.status &&
                problemCode == that.problemCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, status, problemCode);
    }

    @Override
    public String toString() {
        return "TransactionUpdateDto{" +
                "transactionId=" + transactionId +
                ", status=" + status +
                ", problemCode=" + problemCode +
                ", date=" + date +
                '}';
    }
}
