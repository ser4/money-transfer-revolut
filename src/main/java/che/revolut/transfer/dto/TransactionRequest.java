package che.revolut.transfer.dto;

import java.util.Objects;

public class TransactionRequest {
    private int accountFrom;
    private int accountTo;
    private int amountInCents;
    private String webHookUrl;

    public String getWebHookUrl() {
        return webHookUrl;
    }

    public void setWebHookUrl(String webHookUrl) {
        this.webHookUrl = webHookUrl;
    }

    public int getAccountFrom() {
        return accountFrom;
    }

    public int getAccountTo() {
        return accountTo;
    }

    public int getAmountInCents() {
        return amountInCents;
    }

    public void setAccountFrom(int accountFrom) {
        this.accountFrom = accountFrom;
    }

    public void setAccountTo(int accountTo) {
        this.accountTo = accountTo;
    }

    public void setAmountInCents(int amountInCents) {
        this.amountInCents = amountInCents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionRequest that = (TransactionRequest) o;
        return accountFrom == that.accountFrom &&
                accountTo == that.accountTo &&
                amountInCents == that.amountInCents;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountFrom, accountTo, amountInCents);
    }
}
