package che.revolut.transfer.dto;

import che.revolut.transfer.account.Account;

public class AccountDto {
    private int id;
    private int balanceInCents;

    public AccountDto(Account account) {
        this.id = account.getId();
        this.balanceInCents = account.getBalanceInCents();
    }

    public int getId() {
        return id;
    }


    public int getBalanceInCents() {
        return balanceInCents;
    }

    public AccountDto() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBalanceInCents(int balanceInCents) {
        this.balanceInCents = balanceInCents;
    }
}
