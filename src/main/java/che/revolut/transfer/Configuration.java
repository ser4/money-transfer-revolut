package che.revolut.transfer;

import che.revolut.transfer.account.AccountInMemoryRepository;
import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.transaction.TransactionProcessor;
import che.revolut.transfer.transaction.repo.TransactionInMemoryRepository;
import che.revolut.transfer.transaction.repo.TransactionRepository;
import che.revolut.transfer.transaction.service.TransactionExecutor;
import che.revolut.transfer.transaction.service.TransactionService;
import che.revolut.transfer.transaction.service.TransactionValidator;
import che.revolut.transfer.transaction.service.WebHookSender;
import che.revolut.transfer.transaction.service.lock.LockAcquirer;
import che.revolut.transfer.transaction.service.lock.TransactionLockService;

import java.util.concurrent.Executors;

enum Configuration {
    INSTANCE;

    private static final int TRIES = 100;
    private static final long PAUSE = 1;

    private final AccountRepository accountRepository = new AccountInMemoryRepository();
    private final TransactionRepository transactionRepository = new TransactionInMemoryRepository();
    private final TransactionProcessor transactionProcessor;

    Configuration() {
        final WebHookSender webHookSender = new WebHookSender();
        final LockAcquirer lockAcquirer = new LockAcquirer(accountRepo(), TRIES, PAUSE);
        final TransactionService transactionService = new TransactionService(transactionRepository, webHookSender);
        final TransactionValidator validator = new TransactionValidator(accountRepo(), transactionService);
        final TransactionExecutor executor = new TransactionExecutor(accountRepo(), transactionService);
        final TransactionLockService locker = new TransactionLockService(transactionService, lockAcquirer, accountRepo());
        transactionProcessor = new TransactionProcessor(transactionService, locker, validator, executor,
                Executors.newCachedThreadPool());
    }

    AccountRepository accountRepo() {
        return accountRepository;
    }

    TransactionRepository transactionRepo() {
        return transactionRepository;
    }

    TransactionProcessor transactionProcessor() {
        return transactionProcessor;
    }
}
