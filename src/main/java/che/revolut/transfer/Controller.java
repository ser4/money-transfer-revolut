package che.revolut.transfer;

import che.revolut.transfer.account.Account;
import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.dto.AccountDto;
import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.dto.TransactionUpdateDto;
import che.revolut.transfer.transaction.TransactionProcessor;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import che.revolut.transfer.transaction.repo.TransactionRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("bank")
@Produces(MediaType.APPLICATION_JSON)
public class Controller {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionProcessor transactionProcessor;

    public Controller() {
        this(Configuration.INSTANCE.accountRepo(), Configuration.INSTANCE.transactionRepo(), Configuration.INSTANCE.transactionProcessor());
    }

    private Controller(AccountRepository accountRepository, TransactionRepository transactionRepository, TransactionProcessor transactionProcessor) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        this.transactionProcessor = transactionProcessor;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String serverUp() {
        return "Server is Up!";
    }

    @GET
    @Path("accounts")
    public List<Integer> accounts() {
        return accountRepository.readAllIds();
    }

    @GET
    @Path("account/{id}")
    public Response accountById(@PathParam("id") int id) {
        final Optional<Account> optionalAccount = accountRepository.readById(id);
        return optionalAccount.map(AccountDto::new)
                .map(dto -> Response.status(200).entity(dto).build())
                .orElseGet(() -> Response.status(404).build());
    }

    @POST
    @Path("transfer")
    @Consumes(MediaType.APPLICATION_JSON)
    public TransactionUpdateDto transfer(TransactionRequest transactionRequest) {
        final TransactionUpdate update = transactionProcessor.process(transactionRequest);
        return new TransactionUpdateDto(update);
    }

    @GET
    @Path("transactions")
    public List<Integer> transactions() {
        return transactionRepository.readAllIds();
    }

    @GET
    @Path("transaction/{id}")
    public Response transactionById(@PathParam("id") int id) {
        final List<TransactionUpdate> transactionUpdates = transactionRepository.readById(id);
        if (transactionUpdates.isEmpty()) {
            return Response.status(404).build();
        }
        final List<TransactionUpdateDto> dtos = transactionUpdates.stream()
                .map(TransactionUpdateDto::new)
                .collect(Collectors.toList());
        return Response.status(200).entity(dtos).build();
    }

    // to test webhook
    @POST
    @Path("webhook")
    @Consumes(MediaType.APPLICATION_JSON)
    public void hookListener(String json) {
        System.out.println(json);
    }
}
