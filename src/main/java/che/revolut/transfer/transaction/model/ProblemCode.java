package che.revolut.transfer.transaction.model;

public enum ProblemCode {
    NEGATIVE_OR_ZERO_AMOUNT, NOT_EXISTING_FROM_ACCOUNT, NOT_EXISTING_TO_ACCOUNT,
    NOT_ENOUGH_MONEY, SAME_ACCOUNT, NOT_LOCKED
}
