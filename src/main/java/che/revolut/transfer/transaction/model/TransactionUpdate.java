package che.revolut.transfer.transaction.model;

import che.revolut.transfer.dto.TransactionRequest;

import java.util.Date;
import java.util.Objects;

public class TransactionUpdate {
    private final int transactionId;
    private final Status status;
    private final ProblemCode problemCode;
    private final TransactionRequest transactionRequest;
    private final Date date = new Date();

    public TransactionUpdate(int transactionId, Status status, TransactionRequest transactionRequest) {
        this(transactionId, status, null, transactionRequest);
    }

    public TransactionUpdate(int transactionId, Status status, ProblemCode problemCode, TransactionRequest transactionRequest) {
        this.transactionId = transactionId;
        this.status = status;
        this.problemCode = problemCode;
        this.transactionRequest = transactionRequest;
    }

    public static TransactionUpdate of(TransactionUpdate update, ProblemCode problemCode) {
        return new TransactionUpdate(update.transactionId, Status.PROBLEM, problemCode, update.transactionRequest);
    }

    public static TransactionUpdate of(TransactionUpdate update, Status status) {
        return new TransactionUpdate(update.transactionId, status, null, update.transactionRequest);
    }

    public int getTransactionId() {
        return transactionId;
    }

    public Status getStatus() {
        return status;
    }

    public ProblemCode getProblemCode() {
        return problemCode;
    }

    public Date getDate() {
        return date;
    }

    public TransactionRequest getTransactionRequest() {
        return transactionRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionUpdate update = (TransactionUpdate) o;
        return transactionId == update.transactionId &&
                status == update.status &&
                problemCode == update.problemCode &&
                Objects.equals(transactionRequest, update.transactionRequest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, status, problemCode, transactionRequest);
    }
}
