package che.revolut.transfer.transaction.model;

public enum Status {
    CREATED, LOCKED, PROBLEM(true), VALIDATED(true), COMPLETED(true), UNLOCKED;

    private final boolean isExternal;

    Status(boolean isExternal) {
        this.isExternal = isExternal;
    }

    Status() {
        this.isExternal = false;
    }

    public boolean isExternal() {
        return isExternal;
    }
}
