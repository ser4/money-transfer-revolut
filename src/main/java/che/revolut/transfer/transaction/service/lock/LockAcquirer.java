package che.revolut.transfer.transaction.service.lock;

import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.TransactionUpdate;

public class LockAcquirer {

    private final AccountRepository accountRepository;
    private final int maxTries;
    private final long pause;

    public LockAcquirer(AccountRepository accountRepository, int maxTries, long pause) {
        this.accountRepository = accountRepository;
        this.maxTries = maxTries;
        this.pause = pause;
    }

    boolean acquire(TransactionUpdate update) {
        final TransactionRequest request = update.getTransactionRequest();
        int tries = 0;
        boolean lockAcquired = false;
        while (!lockAcquired && tries < maxTries) {
            sleepLongerEveryTry(tries);
            lockAcquired = accountRepository.tryLock(request.getAccountFrom(), request.getAccountTo());
            tries++;
        }
        return lockAcquired;
    }

    private void sleepLongerEveryTry(int tries) {
        try {
            Thread.sleep(tries * pause);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
