package che.revolut.transfer.transaction.service;

import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.dto.TransactionUpdateDto;
import che.revolut.transfer.transaction.model.TransactionUpdate;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

public class WebHookSender {
    private final Client client = ClientBuilder.newClient();

    void send(TransactionUpdate transactionUpdate) {
        final TransactionRequest transactionRequest = transactionUpdate.getTransactionRequest();
        final TransactionUpdateDto dto = new TransactionUpdateDto(transactionUpdate);
        client.target(transactionRequest.getWebHookUrl())
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(dto));
    }
}
