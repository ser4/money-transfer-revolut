package che.revolut.transfer.transaction.service.lock;

import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import che.revolut.transfer.transaction.service.TransactionService;

public class TransactionLockService {

    private final TransactionService transactionService;
    private final LockAcquirer lockAcquirer;
    private final AccountRepository accountRepository;

    public TransactionLockService(TransactionService transactionService, LockAcquirer lockAcquirer,
                                  AccountRepository accountRepository) {
        this.transactionService = transactionService;
        this.lockAcquirer = lockAcquirer;
        this.accountRepository = accountRepository;
    }

    public void executeWithLock(TransactionUpdate update, Runnable r) {
        if (lock(update)) {
            try {
                r.run();
            } finally {
                release(update);
            }
        }
    }

    private boolean lock(TransactionUpdate update) {
        boolean lockAcquired = lockAcquirer.acquire(update);
        if (!lockAcquired) {
            transactionService.failTransaction(update, ProblemCode.NOT_LOCKED);
        } else {
            transactionService.updateTransaction(update, Status.LOCKED);
        }
        return lockAcquired;
    }

    private void release(TransactionUpdate update) {
        final TransactionRequest request = update.getTransactionRequest();
        accountRepository.releaseLock(request.getAccountFrom(), request.getAccountTo());
        transactionService.updateTransaction(update, Status.UNLOCKED);
    }
}