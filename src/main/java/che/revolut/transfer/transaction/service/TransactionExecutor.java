package che.revolut.transfer.transaction.service;

import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;

public class TransactionExecutor {
    private final AccountRepository accountRepository;
    private final TransactionService transactionService;


    public TransactionExecutor(AccountRepository accountRepository, TransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.transactionService = transactionService;
    }

    public void execute(TransactionUpdate transactionUpdate) {
        final TransactionRequest request = transactionUpdate.getTransactionRequest();
        accountRepository.transferMoney(request.getAccountFrom(), request.getAccountTo(), request.getAmountInCents());
        transactionService.updateTransaction(transactionUpdate, Status.COMPLETED);
    }
}
