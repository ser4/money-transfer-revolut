package che.revolut.transfer.transaction.service;

import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import che.revolut.transfer.transaction.repo.TransactionRepository;

public class TransactionService {
    private final TransactionRepository transactionRepository;
    private final WebHookSender webHookSender;

    public TransactionService(TransactionRepository transactionRepository, WebHookSender webHookSender) {
        this.transactionRepository = transactionRepository;
        this.webHookSender = webHookSender;
    }

    public TransactionUpdate createTransaction(TransactionRequest transactionRequest) {
        return transactionRepository.createTransaction(transactionRequest);
    }

    public void failTransaction(TransactionUpdate previousUpdate, ProblemCode problemCode) {
        final TransactionUpdate update = TransactionUpdate.of(previousUpdate, problemCode);
        updateTransaction(update);
    }

    public void updateTransaction(TransactionUpdate previousUpdate, Status status) {
        final TransactionUpdate update = TransactionUpdate.of(previousUpdate, status);
        updateTransaction(update);
    }

    private void updateTransaction(TransactionUpdate transactionUpdate) {
        if (transactionUpdate.getStatus().isExternal()) {
            webHookSender.send(transactionUpdate);
        }
        transactionRepository.updateTransaction(transactionUpdate);
    }
}
