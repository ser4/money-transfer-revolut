package che.revolut.transfer.transaction.service;

import che.revolut.transfer.account.AccountRepository;
import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.ProblemCode;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;

import java.util.Optional;

public class TransactionValidator {

    private final AccountRepository accountRepository;
    private final TransactionService transactionService;

    public TransactionValidator(AccountRepository accountRepository, TransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.transactionService = transactionService;
    }

    public boolean validate(TransactionUpdate transactionUpdate) {
        final TransactionRequest request = transactionUpdate.getTransactionRequest();
        final Optional<ProblemCode> problemCode = accountRepository.validateTransfer(request.getAccountFrom(),
                request.getAccountTo(), request.getAmountInCents());

        final boolean validated = !problemCode.isPresent();
        if (!validated) {
            transactionService.failTransaction(transactionUpdate, problemCode.get());
        } else {
            transactionService.updateTransaction(transactionUpdate, Status.VALIDATED);
        }
        return validated;
    }
}
