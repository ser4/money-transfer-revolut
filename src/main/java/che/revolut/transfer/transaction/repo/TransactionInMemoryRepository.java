package che.revolut.transfer.transaction.repo;

import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.Status;
import che.revolut.transfer.transaction.model.TransactionUpdate;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class TransactionInMemoryRepository implements TransactionRepository {
    private final AtomicInteger sequence = new AtomicInteger();
    private final Map<Integer, List<TransactionUpdate>> transactionHistory = new ConcurrentHashMap<>();

    @Override
    public List<Integer> readAllIds() {
        return new ArrayList<>(transactionHistory.keySet());
    }

    @Override
    public List<TransactionUpdate> readById(int transactionId) {
        return Optional.ofNullable(transactionHistory.get(transactionId))
                .orElse(Collections.emptyList());
    }

    @Override
    public TransactionUpdate createTransaction(TransactionRequest transactionRequest) {
        final int transactionId = sequence.addAndGet(1);
        final TransactionUpdate update = new TransactionUpdate(transactionId, Status.CREATED, transactionRequest);
        List<TransactionUpdate> transactionUpdates = new ArrayList<>();
        transactionUpdates.add(update);
        transactionHistory.put(transactionId, transactionUpdates);
        return update;
    }

    @Override
    public void updateTransaction(TransactionUpdate update) {
        final List<TransactionUpdate> transactionUpdates = transactionHistory.get(update.getTransactionId());
        transactionUpdates.add(update);
    }
}
