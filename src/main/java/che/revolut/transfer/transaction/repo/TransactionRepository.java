package che.revolut.transfer.transaction.repo;

import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.TransactionUpdate;

import java.util.List;

public interface TransactionRepository {

    List<Integer> readAllIds();

    List<TransactionUpdate> readById(int transactionId);

    TransactionUpdate createTransaction(TransactionRequest transactionRequest);

    void updateTransaction(TransactionUpdate transactionUpdate);
}
