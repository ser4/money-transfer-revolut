package che.revolut.transfer.transaction;

import che.revolut.transfer.dto.TransactionRequest;
import che.revolut.transfer.transaction.model.TransactionUpdate;
import che.revolut.transfer.transaction.service.TransactionExecutor;
import che.revolut.transfer.transaction.service.TransactionService;
import che.revolut.transfer.transaction.service.TransactionValidator;
import che.revolut.transfer.transaction.service.lock.TransactionLockService;

import java.util.concurrent.ExecutorService;

public class TransactionProcessor {

    private final ExecutorService executor;
    private final TransactionService transactionService;
    private final TransactionLockService transactionLockService;
    private final TransactionValidator transactionValidator;
    private final TransactionExecutor transactionExecutor;

    public TransactionProcessor(TransactionService transactionService, TransactionLockService transactionLockService,
                                TransactionValidator transactionValidator, TransactionExecutor transactionExecutor,
                                ExecutorService executorService) {
        this.transactionService = transactionService;
        this.transactionLockService = transactionLockService;
        this.transactionValidator = transactionValidator;
        this.transactionExecutor = transactionExecutor;
        this.executor = executorService;
    }


    public TransactionUpdate process(TransactionRequest transactionRequest) {
        final TransactionUpdate update = transactionService.createTransaction(transactionRequest);
        submitTransactionForExecution(update);
        return update;
    }

    private void submitTransactionForExecution(TransactionUpdate update) {
        executor.submit(() -> process(update));
    }

    void process(TransactionUpdate update) {
        transactionLockService.executeWithLock(update, () -> {
            if (transactionValidator.validate(update)) {
                transactionExecutor.execute(update);
            }
        });
    }
}
