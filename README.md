# README #

### Description ###
Application provides simple API for money transfer. Money transfer is processed asynchronously.
When transfer API is executed, a client receives a response with transaction id, timestamp and Created status.
Further transaction status updates are provided by webhook (provided by a client within a request).
#### Following steps are taken to process transfer ####
* lock account from and account to
* validate transfer possibility (correct accounts, amount, etc.)
* transfer money between accounts
* unlock accounts

### Tests ###
Most of the classes are covered with unit tests. 
#### The application is tested in MainTest. It has following test scenarios: ####
* sanity check that a server is up
* reads money balance for an account
* money transfer between 2 accounts, checks correct balance and ensure that correct transaction updates were saved 
and sent to webhook url
* money transfer between 2 accounts with incorrect account, checks no balance updates and ensure that correct transaction
 updates were saved and sent to webhook url
* concurrent test, 100 callables are sumbitted to transfer money between 2 accounts. Checks correct balance after all
transaction are proccessed

### API ###
* GET http://localhost:8080/bank/accounts
Lists all existing account ids. Example:
[1,2,3,4,5]
* GET http://localhost:8080/bank/account/1
Information about account. Example:
{"id":1,"balanceInCents":1000}
* POST http://localhost:8080/bank/transfer
Main Transfer API. Request should contain WebHook Url where transaction statuses updates will be send to. 
Request example:
{"accountFrom":2,"accountTo":1,"amountInCents":10, "webHookUrl":"http://localhost:8080/bank/webhook/"}
Response example with information that a transaction was submitted:
{"transactionId": 11,"status": "CREATED","problemCode": null,"date": "2019-02-08 17:53:19.955"}
Update example sent to webhook url:
{"transactionId": 5,"status": "PROBLEM","problemCode": "NOT_EXISTING_TO_ACCOUNT","date": "2019-02-08 14:53:19.955"}
* GET http://localhost:8080/bank/transactions 
List of transaction ids. Example:
[1,2,3,4,5,6]
* GET http://localhost:8080/bank/transaction/1
All transaction status updates. Example:
[{"transactionId":1,"status":"CREATED","problemCode":null,"date":"2019-02-08 18:16:25.888"},
{"transactionId":1,"status":"LOCKED","problemCode":null,"date":"2019-02-08 18:16:25.892"},
{"transactionId":1,"status":"PROBLEM","problemCode":"SAME_ACCOUNT","date":"2019-02-08 18:16:25.892"},
{"transactionId":1,"status":"UNLOCKED","problemCode":null,"date":"2019-02-08 18:16:25.892"}]

### How do I get set up? ###

* Check out source code
* For build and test execution please execute:
gradlew clean build
* For server start please execute:
gradle run
